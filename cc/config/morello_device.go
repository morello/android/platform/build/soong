// Copyright 2020 Arm Ltd. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package config

import (
	"strings"

	"android/soong/android"
)

var (
	MorelloCflags = []string{
		// Help catch common 32/64-bit errors.
		"-Werror=implicit-function-declaration",
		"-Werror=cheri-provenance-pedantic",
		"-Werror=cheri-provenance",
		"-Werror=cheri-capability-misuse",

		// fstack-protector is unnecessary in Purecap mode,
		// so it is automatically disabled by the compiler,
		// and the warning is issued instead. Disabling
		// the warning since stack protector being disabled is expected outcome:
		// we avoid explicitly disabling stack protector for Morello
		// in every local module configuration enabling it.
		"-Wno-stack-protector-purecap-ignored",

		"-march=morello",
		"-mabi=purecap",
		"-mcpu=rainier",
	}

	MorelloLldflags = []string{
		"-fuse-ld=lld",
		"-Wl,-z,max-page-size=4096",
	}

	MorelloCppflags = []string{}
)

const (
	MorelloGccVersion = "4.9"
)

func init() {
	pctx.StaticVariable("MorelloGccVersion", MorelloGccVersion)

	pctx.SourcePathVariable("MorelloGccRoot",
		"prebuilts/gcc/${HostPrebuiltTag}/aarch64/aarch64-linux-android-${MorelloGccVersion}")

	pctx.StaticVariable("MorelloLldflags", strings.Join(MorelloLldflags, " "))
	pctx.StaticVariable("MorelloIncludeFlags", bionicHeaders("arm64"))

	pctx.StaticVariable("MorelloClangCflags", strings.Join(ClangFilterUnknownCflags(MorelloCflags), " "))
	pctx.StaticVariable("MorelloClangLldflags", strings.Join(ClangFilterUnknownCflags(MorelloLldflags), " "))
	pctx.StaticVariable("MorelloClangCppflags", strings.Join(ClangFilterUnknownCflags(MorelloCppflags), " "))
}

type toolchainMorello struct {
	toolchainC64

	lldflags             string
}

func (t *toolchainMorello) Name() string {
	return "morello"
}

func (t *toolchainMorello) GccRoot() string {
	return "${config.MorelloGccRoot}"
}

func (t *toolchainMorello) GccTriple() string {
	return "aarch64-linux-android"
}

func (t *toolchainMorello) GccVersion() string {
	return MorelloGccVersion
}

func (t *toolchainMorello) IncludeFlags() string {
	return "${config.MorelloIncludeFlags}"
}

func (t *toolchainMorello) ClangTriple() string {
	return t.GccTriple()
}

func (t *toolchainMorello) ClangCflags() string {
	return "${config.MorelloClangCflags}"
}

func (t *toolchainMorello) ClangCppflags() string {
	return "${config.MorelloClangCppflags}"
}

func (t *toolchainMorello) ClangLldflags() string {
	return t.lldflags
}

func (t *toolchainMorello) ClangLdflags() string {
	return "-lld-should-be-used--see-build-soong-cc-config-morello-device"
}

func (t *toolchainMorello) ToolchainClangLdflags() string {
	return t.ClangLldflags()
}

func MorelloToolchainFactory(arch android.Arch) Toolchain {
	return &toolchainMorello{
		lldflags: "${config.MorelloLldflags}",
	}
}

func init() {
	registerToolchainFactory(android.Android, android.Morello, MorelloToolchainFactory)
}
